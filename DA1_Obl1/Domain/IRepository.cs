﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public interface IRepository
    {
        List<IComponent> GetRootTrees();
        void AddRootTree(IComponent component);
        List<IDeviceType> GetDeviceTypes();
        void AddDeviceTypeToList(IDeviceType p);
        bool DeviceTypesHasDuplicate();
        bool RootTreesHasDuplicate();
        IComponent GetComponentByName(string componentName);
    }
}
