﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Repository : IRepository
    {
        List<IComponent> rootTrees;
        List<IDeviceType> deviceTypes;

        public void AddDeviceTypeToList(IDeviceType deviceType)
        {
            throw new NotImplementedException();
        }

        public void AddRootTree(IComponent component)
        {
            throw new NotImplementedException();
        }

        public List<IDeviceType> GetDeviceTypes()
        {
            throw new NotImplementedException();
        }

        public List<IComponent> GetRootTrees()
        {
            throw new NotImplementedException();
        }

        public bool DeviceTypesHasDuplicate()
        {
            throw new NotImplementedException();
        }

        public bool RootTreesHasDuplicate()
        {
            throw new NotImplementedException();
        }

        public IComponent GetComponentByName(string componentName)
        {
            throw new NotImplementedException();
        }
    }
}
