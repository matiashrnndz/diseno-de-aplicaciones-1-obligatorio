﻿namespace Domain
{
    public interface IComponent
    {
        void SetName(string name);
        string getName();
    }
}