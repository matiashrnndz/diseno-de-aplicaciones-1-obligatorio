﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Domain;

namespace DomainTest
{
    [TestClass]
    public class RepositoryTest : TestSetup
    {
        [TestMethod]
        public void RootTreesListisNotNullTest()
        {
            Assert.IsNotNull(InitRepository().GetRootTrees());
        }

        [TestMethod]
        public void RootTreesListIsEmptyTest()
        {
            Assert.AreEqual(0, InitRepository().GetRootTrees().Count);
        }

        [TestMethod]
        public void RootTreesListContainsOneElementTest()
        {
            IRepository repository = InitRepository();

            repository.AddRootTree(InitComponent());

            Assert.AreEqual(1, repository.GetRootTrees().Count);
        }

        [TestMethod]
        public void RootTreesListContainsDuplicateTest()
        {
            IRepository repository = InitRepository();

            repository.AddRootTree(InitComponent());
            repository.AddRootTree(InitComponent());

            Assert.IsTrue(repository.RootTreesHasDuplicate());
        }

        [TestMethod]
        public void DeviceTypesListisNotNullTest()
        {
            Assert.IsNotNull(InitRepository().GetDeviceTypes());
        }

        [TestMethod]
        public void DeviceTypesListIsEmptyTest()
        {
            Assert.AreEqual(0, InitRepository().GetDeviceTypes().Count);
        }

        [TestMethod]
        public void DeviceTypesListContainsOneElementTest()
        {
            IRepository repository = InitRepository();

            repository.AddDeviceTypeToList(InitDeviceType());

            Assert.AreEqual(1, repository.GetDeviceTypes().Count);
        }

        [TestMethod]
        public void DeviceTypesListContainsDuplicateTest()
        {
            IRepository repository = InitRepository();

            repository.AddDeviceTypeToList(InitDeviceType());
            repository.AddDeviceTypeToList(InitDeviceType());

            Assert.IsTrue(repository.DeviceTypesHasDuplicate());
        }

        [ExpectedException(typeof(ExceptionComponentEmptyName))]
        [TestMethod]
        public void GetComponentByNameEmptyStringTest()
        {
            InitRepository().GetComponentByName("");
        }

        public void GetComponentByNameMatchStringTest()
        {
            IRepository repository = InitRepository();
            IComponent addedComponent = InitComponent();

            addedComponent.SetName("componentName");

            repository.AddRootTree(addedComponent);

            IComponent returnedComponent = InitRepository().GetComponentByName("componentName");

            Assert.AreEqual("componentName", returnedComponent.getName());
        }

        public void GetComponentByNameNotMatchStringTest()
        {
            IRepository repository = InitRepository();
            IComponent addedComponent = InitComponent();

            addedComponent.SetName("componentName");

            repository.AddRootTree(addedComponent);

            IComponent returnedComponent = InitRepository().GetComponentByName("componentName");

            Assert.AreEqual("otherComponentName", returnedComponent.getName());
        }
    }
}
