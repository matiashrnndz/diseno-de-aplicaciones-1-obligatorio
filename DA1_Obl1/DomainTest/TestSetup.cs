﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace DomainTest
{
    public class TestSetup
    {
        protected IRepository InitRepository()
        {
            return new Repository();
        }

        protected IComponent InitComponent()
        {
            return new Component();
        }

        protected IDeviceType InitDeviceType()
        {
            return new DeviceType();
        }
    }
}
